%rga.m file
%This script is based on the Regional Genetic Algorithm method which is
%used to approximate the discrete global optimum.

pop = 10;                   %population size
p0 = 0.15;                  %fraction of population generated from the full prescribed set A
s = 20;                     %number at which algorithm stops after no further improvement
p_manip_mut_up = 0.02;      %probability of manipulated mutation upwards
p_manip_mut_down = 0.02;    %probability of manipulated mutation downwards
p_std_mut = 0.01;           %probablity of standard mutation
p_co = 0.8;                 %probablity of cross-over
nc = 1;                     %number of cross-over points
n = 1;                      %number of dimensions

%Theoretical equation
syms a b c x
f = sym('a*x^2 + b*x + c')
a1 = 2;
b1 = 3;
c1 = -1;

%find theoretical (analytic) solution
f_d1 = diff(f);
f_d1 == 0;
optima = solve(f_d1,x);
optima_val = subs(optima,[a b c],[a1 b1 c1])
pretty(optima)

%determine if maximum or minimum
f_d2 = diff(f_d1);
f_d2_at_opt = subs(f_d2, a1);

if f_d2_at_opt > 0
    minimum = 1;
end


%REGIONAL GENETIC ALGORITHM
%discrete increments
inc = 0.02;

%create prescribed set A
A = [-2*abs(optima_val):inc:2*abs(optima_val)];
range = size(A);
length = range(1,2);

generation = randi(length-1,pop,1);

%GENERATE PARENTS
for i = 1:pop           %run trough population
    for j = 1:n         %for an n number of variables in this optimization
    generation(i,j) = A(generation(i,j));
    end
end 

generation

rank = zeros(pop,4);
%column 1      %column 2       %column 3                    %column 4 %c5
%rank          %value          %generation_index_number     %fitness  %f(x)

%create russian roulette
sum = 0;
for i = 1:pop           %run trough population
    fitness = (2*(pop+1-i))/(pop*pop + pop);
    sum = sum + fitness;
    rank(i,4) = sum;
end

improvement_counter = 0;
while improvement_counter < 200
    %if improvement_counter == s
  
    %RANKING (only setup for 1-dimensional variable problem)
    for i = 1:pop           %run trough population
        rank(i,2) = subs(f,[a b c x],[a1 b1 c1 generation(i,1)]);
        rank(i,3) = i;
        rank(i,5) = generation(i);
        %sort
    end
    
    rank = sortrows(rank,2);
    for i = 1:pop           %run trough population
        rank(i,1) = i;
    end 
    
    %SELECTION
    pairs = zeros(pop/2,2);
    pair_index = 1;
    selection1 = 0;
    selection2 = 0;
    
    while pair_index-1 < pop/2
        selection1 = 0;
        selection2 = 0;
        
        r = rand();
        for i = 1:pop           %run trough population
            if r < rank(i,4)
                selection1 = i;
                pairs(pair_index,1) = selection1;
                break
            end
            %if selection1 ~= 0
            %    break %break the for i = 1:pop loop
            %end
            
        end
        
        while selection2 == 0
            r = rand();
            for j = 1:pop
                if r < rank(j,4)
                    if selection1 ~= j      %cannot have a parent that reproduces with itself
                        selection2 = j;
                        pairs(pair_index,2) = selection2;
                        pair_index = pair_index + 1;
                        break %break the for j = 1:pop loop
                    end
                end
            end
            
            if selection2 ~= 0
                break %break the while selection2 == 0 loop
            end
            
        end
        
        
        %pairs(i,1) = selection1;
        %pairs(j,2) = selection2;
        selection1 = 0;
        selection2 = 0;
    end
    
    %CROSS-OVER
    for i = 1:pop/2
        r = rand();
        if r < p_co
            %cross-over shall occur for a specific pair
            %determine random cross-over point (for the moment we will assume 1
            %dimensional problem
            %r = rand();
            generation(i) = generation(rank(pairs(i,1),3));
            generation(i+1) = generation(rank(pairs(i,2),3));
            
        else
            continue %leave the current pair unchanged and continue with crossover for next pair
        end
    end
    
    
    %MUTATION
    for i = 1:pop
        
        %standard mutation
        r = rand();
        if r < p_std_mut
            %mutation is tested once for each child
            %determine random mutation (for the moment we will assume 1
            %dimensional problem
            %r = rand();
            generation(i) = randi(length-1);
            generation(i) = A(generation(i));
        end
        
        %manipulated mutation (upwards)
        r = rand();
        if r < p_manip_mut_up
            %mutation is tested once for each child
            %determine random mutation (for the moment we will assume 1
            %dimensional problem
            %r = rand();
            up_val = generation(i) + inc;
            generation(i) = up_val;
        end
        
        %manipulated mutation (downwards)
        r = rand();
        if r < p_manip_mut_down
            %mutation is tested once for each child
            %determine random mutation (for the moment we will assume 1
            %dimensional problem
            %r = rand();
            down_val = generation(i) - inc;
            generation(i) = down_val;
        end        
        
    end
        
    improvement_counter = improvement_counter + 1;
end

    %pairs
    %rank
    %generation
    
    %FINAL RANKING (only setup for 1-dimensional variable problem)
    for i = 1:pop           %run trough population
        rank(i,2) = subs(f,[a b c x],[a1 b1 c1 generation(i,1)]);
        rank(i,3) = i;
        rank(i,5) = generation(i);
        %sort
    end
    
    rank = sortrows(rank,2);
    for i = 1:pop           %run trough population
        rank(i,1) = i;
    end 
    
    rank(:,5)   %print x
    rank(:,2)   %print f(x)