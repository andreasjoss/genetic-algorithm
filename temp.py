
rank = zeros(pop,4)
#column 1      #column 2       #column 3                    #column 4 			#column5
#rank          #value          #generation_index_number     #fitness  			#f(x)

#create russian roulette
sum = 0
for i = 1:pop           #run trough population
    fitness = (2*(pop+1-i))/(pop*pop + pop)
    sum = sum + fitness
    rank(i,4) = sum
end

improvement_counter = 0;
while improvement_counter < 200
    #if improvement_counter == s
  
    #RANKING (only setup for 1-dimensional variable problem)
    for i = 1:pop           #run trough population
        rank(i,2) = subs(f,[a b c x],[a1 b1 c1 generation(i,1)])
        rank(i,3) = i
        rank(i,5) = generation(i)
        #sort
    end
    
    rank = sortrows(rank,2)
    for i = 1:pop           #run trough population
        rank(i,1) = i
    end 
    
    #SELECTION
    pairs = zeros(pop/2,2)
    pair_index = 1
    selection1 = 0
    selection2 = 0
    
    while pair_index-1 < pop/2
        selection1 = 0
        selection2 = 0
        
        r = rand();
        for i = 1:pop           #run trough population
            if r < rank(i,4)
                selection1 = i
                pairs(pair_index,1) = selection1
                break
            end
            #if selection1 ~= 0
            #    break #break the for i = 1:pop loop
            #end
            
        end
        
        while selection2 == 0
            r = rand()
            for j = 1:pop
                if r < rank(j,4)
                    if selection1 ~= j      #cannot have a parent that reproduces with itself
                        selection2 = j
                        pairs(pair_index,2) = selection2
                        pair_index = pair_index + 1
                        break #break the for j = 1:pop loop
                    end
                end
            end
            
            if selection2 ~= 0
                break #break the while selection2 == 0 loop
            end
            
        end
        
        
        #pairs(i,1) = selection1
        #pairs(j,2) = selection2
        selection1 = 0
        selection2 = 0
    end
    
    #CROSS-OVER
    for i = 1:pop/2
        r = rand()
        if r < p_co
            #cross-over shall occur for a specific pair
            #determine random cross-over point (for the moment we will assume 1
            #dimensional problem
            #r = rand()
            generation(i) = generation(rank(pairs(i,1),3))
            generation(i+1) = generation(rank(pairs(i,2),3))
            
        else
            continue #leave the current pair unchanged and continue with crossover for next pair
        end
    end
    
    
    #MUTATION
    for i = 1:pop
        
        #standard mutation
        r = rand();
        if r < p_std_mut
            #mutation is tested once for each child
            #determine random mutation (for the moment we will assume 1
            #dimensional problem
            #r = rand()
            generation(i) = randi(length-1)
            generation(i) = A(generation(i))
        end
        
        #manipulated mutation (upwards)
        r = rand();
        if r < p_manip_mut_up
            #mutation is tested once for each child
            #determine random mutation (for the moment we will assume 1
            #dimensional problem
            #r = rand()
            up_val = generation(i) + inc
            generation(i) = up_val
        end
        
        #manipulated mutation (downwards)
        r = rand();
        if r < p_manip_mut_down
            #mutation is tested once for each child
            #determine random mutation (for the moment we will assume 1
            #dimensional problem
            #r = rand()
            down_val = generation(i) - inc
            generation(i) = down_val
        end        
        
    end
        
    improvement_counter = improvement_counter + 1
end

    #pairs
    #rank
    #generation
    
    #FINAL RANKING (only setup for 1-dimensional variable problem)
    for i = 1:pop           #run trough population
        rank(i,2) = subs(f,[a b c x],[a1 b1 c1 generation(i,1)])
        rank(i,3) = i
        rank(i,5) = generation(i)
        #sort
    end
    
    rank = sortrows(rank,2)
    for i = 1:pop           #run trough population
        rank(i,1) = i
    end 
    
    rank(:,5)   #print x
    rank(:,2)   #print f(x) 
