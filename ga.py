#!/usr/bin/python
#This script is based on the Regional Genetic Algorithm method which is used to approximate the discrete global optimum.

#Author		Andreas Joss
#Student no	16450612
#Date		2015/03/04
#Task 		Advanced Design 814

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)
import argparse
import pylab as pl

##########################################################################################
manip_factor = 2
pop = 40                  		#population size
p0 = 0.15                  		#fraction of population generated from the full prescribed set A
s = 20                     		#number at which algorithm stops after no further improvement
p_manip_mut_up = 0.01*manip_factor      #probability of manipulated mutation upwards
p_manip_mut_down = 0.01*manip_factor    #probability of manipulated mutation downwards
p_std_mut = 0.005*manip_factor          #probablity of standard mutation
p_co = 0.8                		#probablity of cross-over
nc = 1                     		#number of cross-over points
n_dim = 20                  		#number of dimensions
total_runs = 80000
num_attributes = 2			#number of attributes for each creature
elitism = True

##########################################################################################

parser = argparse.ArgumentParser(description="Genetic Algorithm",formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("choice", help="Select example function to test the GA on:\n"+
		    "1 - Rosenbrock function\n"+
		    "2 - Quadric function\n"+
		    "3 - Ackley function\n"+
		    "4 - Rastrigin function\n\n", type=int, choices=[1,2,3,4])					#add a new POSITIONAL argument with a help message included

parser.add_argument("i", help="Enter total number of iterations the algorithm should complete", type=int)	#add a new POSITIONAL argument with a help message included
parser.add_argument("p", help="Enter desired population the algorithm should use", type=int)			#add a new POSITIONAL argument with a help message included
parser.add_argument("n", help="Enter number of dimensions the example problem should have", type=int)		#add a new POSITIONAL argument with a help message included

parser.add_argument("-a","--average", default=1, help="Enter total number of independant runs to calculate average solution", type=int)	#add a new OPTIONAL argument with a help message included

args = parser.parse_args()													#store the argument parsed into the variable "args"

if args.i != None:
  total_runs = args.i
  
if args.p != None:
  pop = args.p
  
if args.n != None:
  n_dim = args.n

#TEST_PARAMETERS
##########################################################################################
#1 Rosenbrock function
if args.choice == 1:
  upperbound = 2.048
  lowerbound = -2.048
  example_string = "Rosenbrock"
  increment = 0.1

#2 Quadric function
elif args.choice == 2:
  upperbound = 100
  lowerbound = -100
  example_string = "Quadric"
  increment = 5

#3 Ackley function
elif args.choice == 3:
  upperbound = 30
  lowerbound = -30
  example_string = "Ackley"
  increment = 0.5

#4 Rastrigin function
elif args.choice == 4:
  upperbound = 5.12
  lowerbound = -5.12
  example_string = "Rastrigin"
  increment = 0.1 #kies dalk 0.2
##########################################################################################

creatures = pl.np.random.uniform(low=lowerbound,high=upperbound,size = (pop,n_dim))		#generate matrix with random FLOAT entries according to a prescribed set A
attributes = pl.np.zeros((pop,num_attributes))
creatures = pl.np.append(attributes,creatures, axis = 1)	#append attributes to the creatures matrix and save it to the creatures variable
pairs = pl.np.zeros((pop/2,2))

##########################################################################################  
#print test TEST_PARAMETERS
print "The following example function will be tested : " + example_string
print "Total number of iterations:	%d" %total_runs
print "Population size:		%d" %pop
print "Dimensions:			%d" %n_dim
print "Independant Runs:		%d\n" %args.average

#TEST_FUNCTIONS
#1 Rosenbrock function
def rosenbrock(x):
  sum = 0
  for i in range (1,int(n_dim/2)+1):		#NB, sum of boundary includes i = n_dim/2
    sum = sum + pl.np.power(100*(x[2*i]-pl.np.power(x[2*i-1],2)),2) + pl.np.power((1-x[2*i-1]),2)
  return sum

def quadric(x):
  sum = 0
  sum_inner = 0
  for i in range (1,n_dim+1):			#NB, sum of boundary includes i = n_dim
    for j in range (1,i+1):
      sum_inner = sum_inner + x[j]		#NB, sum of boundary includes j = i
    sum = sum + pl.np.power(sum_inner,2)
  return sum

def ackley(x):
  sumA = 0
  sumB = 0
  for i in range (1,n_dim+1):			#NB, sum of boundary includes i = n_dim
    sumA = sumA + pl.np.power(x[i],2)
    sumB = sumB + pl.np.cos(2*pl.np.pi*x[i])
    
  return -20*pl.np.exp(-0.2*pl.np.sqrt((1/n_dim)*sumA)) - pl.np.exp((1/n_dim)*sumB) + 20 + pl.np.exp(1)

def rastrigin(x):
  sum = 0
  for i in range (1,n_dim+1):			#NB, sum of boundary includes i = n_dim
    sum = sum + (pl.np.power(x[i],2) - (10*pl.np.cos(2*pl.np.pi*x[i])) + 10)
  return sum

def example(x):
  #these examples require the first array index to be referred as 1 (which Matlab does, but Python refers to the first index as 0)
  #for this reason a temporary array is created to deal with this issue
  for i in range (1,n_dim+1):
    example_temp[i] = x[i-1]
  
  if args.choice == 1:
    return rosenbrock(example_temp)
  elif args.choice == 2:
    return quadric(example_temp)
  elif args.choice == 3:
    return ackley(example_temp)
  elif args.choice == 4:
    return rastrigin(example_temp)

##########################################################################################

#create russian roulette
roulette = pl.np.zeros((pop))
sum = 0
for i in range (1,pop):           #run trough population
    fitness = (2*(pop+1-i))/(pop*pop + pop)
    sum = sum + fitness
    roulette[i-1] = sum
roulette[pop-1]=1
##########################################################################################

mut_std_counter_arr = []
mut_up_counter_arr = []
mut_down_counter_arr = []
iteration_std_ctr = []
iteration_up_ctr = []
iteration_down_ctr = []
example_temp = pl.np.zeros((n_dim+1,1))	#serves as the array which will deal with indexing problem (Artinetively, I could also add another attribute column which could serve as x[0])
mut_std_counter = 0
mut_up_counter = 0
mut_down_counter = 0
std_r_avg = 0
down_r_avg = 0
great_total_counter = 0
best_avg = 0
best_ever = 0
best_fx = []

#append empty list to existing best_fx[] list (thus constructing a 2D list)
for i in range (0,args.average):
  best_fx.append([])
##########################################################################################
for independant_runs in range (0,args.average):
  creatures = pl.np.random.uniform(low=lowerbound,high=upperbound,size = (pop,n_dim))		#generate matrix with random FLOAT entries according to a prescribed set A
  attributes = pl.np.zeros((pop,num_attributes))
  creatures = pl.np.append(attributes,creatures, axis = 1)	#append attributes to the creatures matrix and save it to the creatures variable

  
  iteration = 0
  while iteration < total_runs:
    great_total_counter = great_total_counter + 1
    print pl.np.round((great_total_counter/(total_runs*args.average))*100,0)," percent complete     \r",	#The comma prevents print from adding a newline. (and the spaces will keep the line clear from prior output)
									#Also, don't forget to terminate with a print "" to get at least a finalizing newline!
    iteration = iteration + 1
  
    #RANKING
    for i in range (0,pop):           #run trough population
      creatures[i,num_attributes-1] = example(creatures[i,num_attributes:])
    
    creatures = creatures[pl.np.argsort(creatures[:, num_attributes-1])]					#sort table
    
    if iteration==1 and independant_runs==0:
      best_ever = creatures[0,num_attributes-1]
    elif creatures[0,num_attributes-1]<best_ever:
      best_ever = creatures[0,num_attributes-1] 
      

    for i in range (0,pop):
      creatures[i,num_attributes-2] = roulette[i]							#assign new fitness value for each creature in 2nd column
    
    best_fx[independant_runs].append(creatures[0,num_attributes-1])
 
    
    #SELECTION
    pair_index = 0
    selection1 = -1
    selection2 = -1
    
    while pair_index < pop/2:
      selection1 = -1
      selection2 = -1
        
      r = pl.np.random.rand()			#returns a random float between 0 and 1 if no argument is given
      for i in range (0,pop):           		#run trough population
	if r < creatures[i,num_attributes-2]:
	  selection1 = i
	  pairs[pair_index,0] = selection1
	  break
        
      while selection2 == -1:
	r = pl.np.random.rand()
	for j in range (0,pop):
	  if r < creatures[j,num_attributes-2]:
	    if selection1 != j:     			#cannot have a parent that reproduces with itself
	      selection2 = j				#have successfully found a pair
	      pairs[pair_index,1] = selection2
	      pair_index = pair_index + 1
	      break#j = pop #equivalent to break the for loop
	  
    #CROSSOVER (need to implement n-dimensions)
    j = 0
    for i in range (0,int(pop/2)):
      r = pl.np.random.rand()
      if elitism == True and i == 0:
	i = i + 1
	j = j + 2
      elif r < p_co:
	crossover_point = pl.np.random.randint(low=1,high=n_dim+1,size = (1,1))		#crossover point is randomly selected [low inclusive, high exclusive)
	creatures[j,num_attributes:num_attributes+crossover_point] = creatures[pairs[i,0],num_attributes:num_attributes+crossover_point]
	creatures[j+1,num_attributes:num_attributes+crossover_point] = creatures[pairs[i,0],num_attributes:num_attributes+crossover_point]
	i = i + 1
	j = j + 2
      else:
	i = i + 1
	j = j + 2
    
   
    #MUTATION (need to implement n-dimensions)
    for i in range (0,pop):        
      r = pl.np.random.rand()		#this probability decreases as the number of iterations increases
      if r < (1-(iteration/total_runs))*p_std_mut:	    					#standard mutation
	mutate_this_gene = pl.np.random.randint(low=num_attributes,high=num_attributes+n_dim,size = (1,1))	#mutation point is randomly selected [low inclusive, high exclusive)
	if mutate_this_gene != 0 and elitism == True:
	  creatures[i,mutate_this_gene] = pl.np.random.uniform(low=lowerbound,high=upperbound,size = (1,1))	#FLOAT
	  if independant_runs == 0:
	    mut_std_counter = mut_std_counter + 1
	    mut_std_counter_arr.append(mut_std_counter)
	    iteration_std_ctr.append(iteration-1)
	    #mutation is tested once for each child
       
      r = pl.np.random.rand()		#this probablity increases as the number of iterations decreases
      if r < (iteration/total_runs)*p_manip_mut_up:					#manipulated mutation (upwards)
	if creatures[i,num_attributes] < upperbound + increment:
	  mutate_this_gene = pl.np.random.randint(low=num_attributes,high=num_attributes+n_dim,size = (1,1))	#mutation point is randomly selected [low inclusive, high exclusive)
	  if mutate_this_gene != 0 and elitism == True:
	    creatures[i,mutate_this_gene] = creatures[i,mutate_this_gene] + increment
	    if independant_runs == 0:
	      mut_up_counter = mut_up_counter + 1
	      mut_up_counter_arr.append(mut_up_counter)
	      iteration_up_ctr.append(iteration-1)
	      #mutation is tested once for each child
      else:
	r = pl.np.random.rand()
	if r < (iteration/total_runs)*p_manip_mut_down:					#manipulated mutation (downwards)
	  if creatures[i,num_attributes] > lowerbound - increment:
	    mutate_this_gene = pl.np.random.randint(low=num_attributes,high=num_attributes+n_dim,size = (1,1))	#mutation point is randomly selected [low inclusive, high exclusive)
	    if mutate_this_gene != 0 and elitism == True:
	      creatures[i,mutate_this_gene] = creatures[i,mutate_this_gene] - increment
	      if independant_runs == 0:
		mut_down_counter = mut_down_counter + 1
		mut_down_counter_arr.append(mut_down_counter)
		iteration_down_ctr.append(iteration-1)
		#mutation is tested once for each child
       
      
  #END OF WHILE iteration < total_runs:

  ##########################################################################################      
  #FINAL RANKING
  for i in range (0,pop):           #run trough population
    creatures[i,num_attributes-1] = example(creatures[i,num_attributes:])

  creatures = creatures[pl.np.argsort(creatures[:, num_attributes-1])]					#sort table
  for i in range (0,pop):
    creatures[i,num_attributes-2] = roulette[i]							#assign new fitness value for each creature in 2nd column
    
  ##########################################################################################    
  best_avg = best_avg + creatures[0,num_attributes-1]
  
#END OF independant_runs in range (0,args.average):
best_avg = best_avg/args.average
  


print "Average solution of independant runs f(x) = %.2f\n\nBest solution of independant runs    f(x) = %.2f" %(best_avg,best_ever)

  
#Maximize output window
'''
### for 'TkAgg' backend5
pl.switch_backend('TkAgg')
print '#1 Backend:',pl.get_backend()
figManager = pl.get_current_fig_manager()
figManager.resize(*figManager.window.maxsize())			#linux
#figManager.window.state('zoomed') 				#works fine on Windows!	
'''
'''
### for 'Qt4Agg' backend
pl.switch_backend('QT4Agg')
print '#2 Backend:',pl.get_backend()
figManager = pl.get_current_fig_manager()
figManager.window.showMaximized()
'''

fig1 = pl.figure(1)
pl.xlabel("Iteration number")
pl.ylabel("Number of mutations")
pl.title("Number of mutations vs. iteration number")

pl.plot(iteration_std_ctr,mut_std_counter_arr,label="Number of standard mutations")
pl.plot(iteration_up_ctr,mut_up_counter_arr,label="Number of manipulated mutations upwards")
pl.plot(iteration_down_ctr,mut_down_counter_arr,label="Number of manipulated mutations downwards")
pl.legend(loc=2)
F = pl.gcf()
F.savefig("./graphs/4rastrigin/Rastrigin2A.pdf",bbox_inches='tight',format='PDF',pad_inches=0.1)


fig2 = pl.figure(2)
pl.title("f(x) vs. number of iterations executed")

pl.xlabel("Iteration number")
pl.ylabel("f(x)")
pl.yscale('log')
for i in range (0,args.average):
  pl.plot(range(0,total_runs),best_fx[i], label = "Run %d: Optimum f(x)" %(i+1))

pl.figtext(0.40,0.882, "Total iterations: %d\nPopulation: %d\nDimensions: %d\n" %(total_runs,pop,n_dim),fontsize=10, bbox=dict(facecolor = 'white', alpha=1, linewidth = 1, edgecolor = 'black'), horizontalalignment = 'left',verticalalignment='top')
l = pl.legend(loc=1)# draggable(state=True))
l.draggable(state=True)
pl.figtext(0.1375,0.2375, "Average solution of independant runs\nf(x) = %.2f\n\nBest solution of independant runs\nf(x) = %.2f" %(best_avg,best_ever),fontsize=10, bbox=dict(facecolor = 'white', alpha=1, linewidth = 1, edgecolor = 'black'), horizontalalignment = 'left',verticalalignment='top')


params = {'legend.fontsize': 10 ,
          'legend.linewidth': 2}
pl.rcParams.update(params)			#Then do the plot afterwards. There are a ton of other rcParams, they can also be set in the matplotlibrc file.

F = pl.gcf()
F.savefig("./graphs/4rastrigin/Rastrigin2B.pdf",bbox_inches='tight',format='PDF',pad_inches=0.1)

pl.show()