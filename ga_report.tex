\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}					%for advanced mathmematical formulas
\usepackage{graphicx}					%always use this package if you want to insert graphics into your report
\usepackage{fullpage}

\graphicspath{{./graphs/1rosenbrock/}{./graphs/2quadric/}{./graphs/3ackley/}{./graphs/4rastrigin/}}

% Title Page
\title{\Huge{Genetic Algorithm Task}\\[7cm]Advanced Design 814\\[2cm]}

\author{\Large Andreas Joss\\[0.5cm]16450612}
\date{\today}

\begin{document}
\maketitle

\newpage
\section{Introduction}
This report briefly explains the  genetic algorithm. Furthermore, the algorithm is implemented in code and the results thereof are shown and discussed.

\section{Basic Theory}
The genetic algorithm (GA) is based on the biological theory of survival of the fittest [1]. The algorithm attempts to improve the `fitness' of designs which are evaluated using an objective cost function. At first, all possible designs (members of the population) are given random, but plausible, genes. Genetic operators such as selection, cross-over and mutation are used to ensure that this population $p$ of possible designs $i$ improve their fitness as the next generations are created [1].  

\section{Implementation}
When new off-spring is created from the previous generation, genetic operators are implemented in the following particular order: selection; cross-over; mutation.

\subsection{Selection}
Selection is the process where members $i$ of the current population $p$ are selected to be candidates of a possible cross-over process which follows selection. The probability of a member being selected is proportional to fitness of the member relative to the fitness of other members in the population. Thus it is first necessary to calculate the relative rank of each member according to some cost function. Thereafter the fitness of each member $i$ is expressed as [1]:
\begin{equation}\label{eq:1}
 \frac{2(p +  1 - i)^{c}}{p^{2} + p}
\end{equation}
where the sum of all fitness values equals unity and $c$ is usually assumed $c=1$ [1]. A random real number within $[0,1]$ determines which member is selected. For instance, the highest ranking member is selected when the generated random number lies within $[0,\text{ fitness of no. 1 ranked member})$. The second highest ranking member tested only after it is found that the random number does not lie within the boundaries of the aforementioned test. The second highest ranking number is then selected if the random number lies within $[0,\text{ fitness of (no. 1 + no. 2) ranked member})$. The remaining members are tested in order of their respective fitness values accumulated with the previous fitness test conditions.

\subsection{Cross-over}
Cross-over is the process where genes are carried over from the parents to the new born children. This process happens at a high probability and hence only a small fraction of parents are carried over into the new generation unchanged [1]. For this task it is assumed $p_{(c)}=0.8$. The candidates that cross-over could apply to (if a random number falls within the aforementioned high probability) do not necessarily carry over equal amount of genes to a child. For this particular task, a single cross-over point is selected. Genes falling on the left-side of the cross-over point are carried over from the selected Parent 1 and genes to the right of the cross-over point are carried over from the selected Parent 2 to create Child 1. Child 2 is created by swapping the sides of Parent 1 and Parent 2. The following table indicates this process, where crossover occurs between the second and third genes of each parent: \\[0.5cm]

\begin{table}[h]
\centering
\begin{tabular}{l  c  c | c  c  c  c  c}	%[l] = left justified column,[r] = right justified, [c] = centre column, to place lines between columns, use the "bar" symbol |
Selected parent 1 & 16 & 12 & 7 & 3 & 1 & 19 & 5\\
Selected parent 2 & 8 & 3 & 6 & 5 & 2 & 14 & 17\\
Child 1 & 8 & 3 & 7 & 3 & 1 & 19 & 5\\
Child 2 & 16 & 12 & 6 & 5 & 2 & 14 & 17\\
\end{tabular}
\end{table}

\subsection{Mutation}

Throughout this task, a population size of $p=20$ is used. Because this is a relatively small population, a lack of genetic diversity could become problematic if good solutions are desired. Mutation is therefore necessary to prevent loss of diversity [1]. It is applied with a relative low probability, otherwise the algorithm becomes biased towards a random number generator. Only a single gene of a new born child is mutated. Another random number is required to select the gene which should be mutated. Three different mutations are used for this task, namely: standard mutation; manipulated mutation upwards, manipulated mutation downwards.

Standard mutation allows the selected gene of a new born child to be changed into any number within the boundaries of a particular problem. This type of mutation is helpful to prevent a loss of diversity, especially when the current solution is still far from the actual solution. Standard mutation is only tested once for each new born child, and only a single gene is to be altered. The probability of the standard mutation can be decreased as the iterations increase to prevent overshoot when the algorithm's solution becomes closer to the theoretical solution. The following equation is used to gradually decrease the probability of standard mutation:
\begin{equation}\label{eq:2}
 \text{standard mutation probability  } p_{(m) effective} = 1 - \frac{\text{iteration}}{\text{total iterations}} \times p_{(m)}
\end{equation}
where $p_{(m)} = 0.001$\\[0.5cm]

Manipulated mutation allows the selected gene of a new born child to be increased (if upward manipulated mutation is tested) or decreased (if downward manipulated mutation is tested). This type of mutation is complimentary to the aforementioned standard mutation and is helpful when the algorithm's solution is already close to theoretical solution as it helps to refine the genes for the actual solution. The probability of the manipulated mutation can be increased as the iterations increase to ensure that this mutation is more active once refinement of the solution is needed. For this task it is implemented that downward manipulated mutation only occurs if upward manipulated mutation did not occur, and if the probability condition is also satisfied. The following equations are used to gradually increase the probability of both upward and downward manipulated mutation:
\begin{equation}\label{eq:3}
 \text{upward manipulated mutation probability  } p_{(m_{u}) effective} = \frac{\text{iteration}}{\text{total iterations}} \times p_{(m_{u})}
\end{equation}

\begin{equation}\label{eq:4}
 \text{downward manipulated mutation probability  } p_{(m_{d}) effective} = \frac{\text{iteration}}{\text{total iterations}} \times p_{(m_{d})}
\end{equation}

where $p_{(m_{u})}=0.02$ and $p_{(m_{d})}=0.02$\\[0.5cm]

\section{Example Functions}
Example functions are used to evaluate the effectiveness of this particular implementation of the GA. Each graph displays the output of the function for ten independent runs, each with the equal amount of iterations executed. Several parameters for the optimization is shown on the graph figures. The average of the ten independent results are shown on the graph, as well as the best result found considering all ten independent runs. All values of $f(\textbf{x})$ is displayed on a logarithmic scale since the values can initially be very large. The members of the population are given intitial genetic values that fit within the boundaries of $\pm2.048$, $\pm100$, $\pm30$ $\pm5.12$ respectively for each example function. The genes are never allowed to exceed these values.

Four functions are used to evaluate the effectiveness of the GA. The mathematical description of each function is given in this section.
\subsection{Rosenbrock Function}
 \begin{equation}
  f_{0}(x)=\sum_{i=1}^{n/2} (100(x_{2i} - x_{2i-1}^{2})^{2} + (1-x_{2i-1})^{2})
 \end{equation}
\\[0.5cm]
The theoretical solution is at $\textbf{x} = \textbf{1}$ which gives $f(\textbf{1}) = 0$
\subsection{Quadric Function}
\begin{equation}
 f_{1}(x)=\sum_{i=1}^{n}\left(\sum_{j=1}^{i} x_{j} \right)^{2}
\end{equation}
\\[0.5cm]
The theoretical solution is at $\textbf{x} = \textbf{0}$ which gives $f(\textbf{0}) = 0$
\subsection{Ackley Function}
\begin{equation}
 f_{2}(x)=-20\text{exp}\left(-0.2\sqrt{1/n \sum_{i=1}^{n} x_{i}^{2}}\right) -\text{exp}\left(1/n \sum_{i=1}^{n} \cos{(2\pi x_{i}})\right) + 20 + e
\end{equation}
\\[0.5cm]
The theoretical solution is at $\textbf{x} = \textbf{0}$ which gives $f(\textbf{0}) = 0$
\subsection{Rastrigin Function}
\begin{equation}
 f_{3}(x)=\sum_{i=1}^{n} (x_{i}^{2} - 10 \cos{(2\pi x_{i})} + 10)
\end{equation}
\\[0.5cm]
The theoretical solution is at $\textbf{x} = \textbf{0}$ which gives $f(\textbf{0}) = 0$

\section{Results}
The GA is programmed in the Python language, due to future work which will also require extensive Python scripting. The Matplotlib library is used to display the results in a neat and informative manner.

\newpage
\subsection{Rosenbrock Function}
\begin{figure}[h]
\centering
 \includegraphics[scale=0.53]{Rosenbrock7A.pdf} 
 \caption{Counts for each type of mutation for the Rosenbrock function optimized using 10 dimensions, population of 20, and 8000 iterations}
 \label{fig:rosenbrock1}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.53]{Rosenbrock7B.pdf} 
 \caption{Rosenbrock function optimized using 10 dimensions, population of 20, and 8000 iterations}
 \label{fig:rosenbrock2}
\end{figure}

Figure \ref{fig:rosenbrock1} shows the effects of Equation \ref{eq:2}, \ref{eq:3} and \ref{eq:4} to try to increase the effectiveness of the various types of mutations. As expected the number of standard  mutations decreases as the iterations increase and the number of manipulated mutations increase towards the end of the optimization process.  

Figure \ref{fig:rosenbrock2} indicates that the solution is moving towards to the theoretical answer of $f(\textbf{1}) = 0$.

\newpage
\subsection{Quadric Function}
\begin{figure}[h]
\centering
 \includegraphics[scale=0.53]{Quadric1B.pdf} 
 \caption{Quadric function optimized using 10 dimensions, population of 20, and 8000 iterations}
 \label{fig:quadric1}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.53]{Quadric2B.pdf} 
 \caption{Quadric function optimized using 20 dimensions, population of 20, and 8000 iterations}
 \label{fig:quadric2}
\end{figure}

Figure \ref{fig:quadric1} that the GA struggles to converge the Quadric function towards the theoretical answer of $f(\textbf{0}) = 0$. However the GA does manage to improve the initial random solution significantly.

From Figure \ref{fig:quadric2} it is clear that the GA struggles even more than the previous result from Figure \ref{fig:quadric1} due to the increase of the problem dimensions.

\newpage
\subsection{Ackley Function}
\begin{figure}[h]
\centering
 \includegraphics[scale=0.53]{Ackley1B.pdf} 
 \caption{Ackley function optimized using 10 dimensions, population of 20, and 8000 iterations}
 \label{fig:ackley1}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.53]{Ackley2B.pdf} 
 \caption{Ackley function optimized using 20 dimensions, population of 20, and 8000 iterations}
 \label{fig:ackley2}
\end{figure}

Figure \ref{fig:ackley1} shows that the GA almost converges the Ackley function towards the theoretical answer of $f(\textbf{0}) = 0$.

In Figure \ref{fig:ackley2} the dimensions are increased to 20. The figure shows that the GA also almost converges the Ackley function towards the theoretical answer of $f(\textbf{0}) = 0$, but struggles slightly more due to the increased dimension size.

\newpage
\subsection{Rastrigin Function}
\begin{figure}[h]
\centering
 \includegraphics[scale=0.53]{Rastrigin1B.pdf} 
 \caption{Rastrigin function optimized using 10 dimensions, population of 20, and 8000 iterations}
 \label{fig:rastrigin1}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.53]{Rastrigin2B.pdf} 
 \caption{Rastrigin function optimized using 20 dimensions, population of 20, and 8000 iterations}
 \label{fig:rastrigin2}
\end{figure}

Figure \ref{fig:rastrigin1} shows that the GA almost converges the Rastrigin function towards the theoretical answer of $f(\textbf{0}) = 0$.

Figure \ref{fig:rastrigin2} shows that the GA struggles much more to solve the Rastrigin function because of the increased dimension size. However a significant improvement is noticeable from the initial random solutions.

\newpage
\section{Conclusion}
From the results it seems that the GA algorithm is a handy tool to use when a cost function of 10-20 variables need to be optimized. It is evident that the Quadric function is more difficult for the GA algorithm to solve than for instance the Ackley function.  It is also noted that an increase of iterations usually result in better solutions, thus given enough iterations, the desired result seems achievable. Furthermore, during construction and testing of the code, it was interesting to see how the mutation parameters affected the outcome. If the manipulated mutation probability is too large near the end of the optimization process, the solution would oscillate too much around the actual solution. If the standard mutation was too small at the start of the optimization process, the algorithm would converge prematurely.

All considered the GA algorithm is quite tricky to implement but it is a helpful tool to solve a multi-dimensional optimization problem.

\section{Bibliography}
1. Groenwold AA, Stander N, Snyman J.A., Regional Genetic Algorithm. International Journal for Numerical Methods in Engineering 1999; 44:749-766.

\end{document}
